# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include

from rest_framework_jwt.views import obtain_jwt_token

from core.api import SimpleUsersAPIView
from .views import index_view

urlpatterns = [
    url(r'^$', index_view),
    url(r'^users/$', SimpleUsersAPIView.as_view(), name='simple-users'),
    url(r'^token-auth/', obtain_jwt_token, name='token-auth'),
    url(r'^tasks/', include('tasks.urls'))
]
