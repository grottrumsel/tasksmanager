# -*- coding: utf-8 -*-

from rest_framework import generics, serializers
from django.contrib.auth.models import User


class SimpleRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'password')
        extra_kwargs = {
            'password': {'write_only': True, 'min_length': 8}
        }

    def save(self, **kwargs):
        return User.objects.create_user(
            username=self.validated_data['username'],
            password=self.validated_data['password']
        )


class SimpleUsersAPIView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = SimpleRegistrationSerializer
    permission_classes = []
