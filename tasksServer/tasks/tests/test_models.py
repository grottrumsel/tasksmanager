# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
import pytz

import mock

from django.contrib.auth.models import User
from django.test import TestCase

from tasks.models import Task

# test literals
T_TITLE = 'title'
T_DESCR = 'description'


class BaseTaskTestCase(TestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('user_one')

    # shortcut
    def _create_test_task(self, title=None):

        title = title or T_TITLE

        return Task.objects.create(
            title=title,
            description=T_DESCR
        )


class TestTaskModel(BaseTaskTestCase):

    def test_task_creation(self):

        datetime_now = datetime.now(tz=pytz.UTC)
        # mocking built in 'now' to check if auto_add time works
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = datetime_now
            task = self._create_test_task()
            self.assertIsNotNone(task)
            self.assertEqual(task.added, datetime_now)
        task = Task.objects.get(id=task.id)
        self.assertIsNotNone(task)

        # basic test of fields
        expected_fields = ['status', 'added', 'description', 'title', 'doer_id', 'id', 'owner_id']
        self.assertTrue(
            set(expected_fields).issubset(task.__dict__.keys())
        )
        # task.added checked at the beginning
        self.assertIsNotNone(task.id)
        self.assertEqual(task.title, T_TITLE)
        self.assertEqual(task.description, T_DESCR)
        # after creation status is 'UNDONE'
        self.assertEqual(task.status, Task.UNDONE)
        self.assertIsNone(task.owner)
        self.assertIsNone(task.doer)

    def test_tasks_ordering(self):
        # ordering is set for purpose of API to show newest tasks first
        [self._create_test_task('title {}'.format(item)) for item in xrange(3)]
        self.assertEqual(['title 2', 'title 1', 'title 0'], [task.title for task in Task.objects.all()])

    def test_setting_doer_null_on_delete(self):
        self._test_setting_user_null_on_delete('doer')

    def test_setting_owner_null_on_delete(self):
        self._test_setting_user_null_on_delete('owner')

    def _test_setting_user_null_on_delete(self, user_field_name):
        """
        Allows to check both user roles on task: owner and doer if
        their ForeignKey related field will become None after their
        deletion. Distinguished by user_field_name.
        """

        # setting up a task
        task = self._create_test_task()
        self.assertIsNone(getattr(task, user_field_name))
        # assigning user to task
        setattr(task, user_field_name, self.test_user)
        task.save()
        self.assertIsNotNone(getattr(task, user_field_name))
        # checking task from db
        task = Task.objects.get(id=task.id)
        self.assertEqual(getattr(task, user_field_name), self.test_user)
        # deleting user expecting to on_delete mechanism work
        self.test_user.delete()
        task = Task.objects.get(id=task.id)
        # task itself exists but owner is None
        self.assertIsNotNone(task)
        self.assertIsNone(getattr(task, user_field_name))

