# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict, namedtuple
from datetime import datetime
import pytz

import mock

from django.contrib.auth.models import User
from django.test import TestCase

from tasks.models import Task
from tasks.serializers import TaskSerializer, TaskUserSerializer

# test literals
T_TITLE = 'title'
T_DESCR = 'description'

MockRequest = namedtuple('MockRequest', ['user', 'method'])


class TestTaskSerializers(TestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('user_one')
        self.datetime_now = datetime.now(tz=pytz.UTC)

        #preparing date to be mocked
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = self.datetime_now

            self.test_task = Task.objects.create(
                title=T_TITLE,
                owner=self.test_user,
                doer=self.test_user,
                description=T_DESCR
            )

        self.expected_data = {
            u'status': u'U',
            u'added': self.datetime_now.strftime('%Y-%m-%d %H:%M:%S'),
            u'description': u'description',
            u'title': u'title',
            u'doer': TaskUserSerializer(self.test_user).data,  # data itself tested above
            u'status_verbose': u'Undone',
            u'owner': TaskUserSerializer(self.test_user).data,  # data itself tested above
            u'id': 1

        }

    def test_owner_doer_serializer(self):

        serializer = TaskUserSerializer(self.test_user)
        expected_data = {
            u'id': 1, u'username': u'user_one',
        }
        self.assertDictEqual(expected_data, serializer.data)

    def test_task_serializer_from_model(self):
        serializer = TaskSerializer(instance=self.test_task)
        self.assertDictEqual(self.expected_data, serializer.data)

        self.test_task.status = Task.DONE
        serializer = TaskSerializer(instance=self.test_task)
        self.assertEqual(Task.task_status_map[Task.DONE], serializer.data['status_verbose'])

    def _prepare_task_serializer_with_data(self, task_data, method):
        serializer = TaskSerializer(
            data=task_data,
            context={
                'request': MockRequest(user=self.test_user, method=method)
            }
        )
        return serializer

    def test_task_serializer_from_data(self):

        task_data = {
            'title': T_TITLE,
            'description': T_DESCR
        }

        serializer = self._prepare_task_serializer_with_data(task_data, 'POST')  # mocking creation

        self.assertTrue(serializer.is_valid())
        self.assertEqual(Task.objects.count(), 1)
        task_saved = serializer.save()
        task_saved.refresh_from_db()
        self.assertEqual(task_saved.owner, self.test_user)
        self.assertEqual(task_saved.doer, None)
        self.assertEqual(task_saved.status, Task.UNDONE)
        self.assertEqual(task_saved.title, T_TITLE)
        self.assertEqual(task_saved.description, T_DESCR)
        self.assertEqual(Task.objects.count(), 2)

    def test_task_serializer_validations_1(self):
        task_data = {}
        serializer = self._prepare_task_serializer_with_data(task_data, 'POST')  # mocking creation
        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors['description'][0].code, 'required')
        self.assertEqual(serializer.errors['title'][0].code, 'required')

    def test_task_serializer_validations_2(self):
        task_data = {
            'title': 'a'*200,  # > 128
            'description': 'a'*600,  # > 512
            'status': 'YYY',
        }
        serializer = self._prepare_task_serializer_with_data(task_data, 'POST')  # mocking creation
        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors['title'][0].code, 'max_length')
        self.assertEqual(serializer.errors['description'][0].code, 'max_length')
        self.assertEqual(serializer.errors['status'][0].code, 'invalid_choice')
