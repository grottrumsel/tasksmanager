# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from rest_framework import status as status_code  # to avoid confusion with task.status

from tasks.models import Task
from tasks.serializers import TaskSerializer, TaskUserSerializer
from tasks.tests.api.commons import AuthenticatedAPITestCaseMixin, TaskAPIBaseTestCase, T_TITLE, T_DESCR


class TestTaskAPICreate(AuthenticatedAPITestCaseMixin, TaskAPIBaseTestCase):

    create_data = {
        'title': T_TITLE,
        'description': T_DESCR
    }

    def test_object_create(self):
        response = self.client.post(reverse('tasks-list'), self.create_data)
        self.assertEqual(response.status_code, status_code.HTTP_201_CREATED)
        task = Task.objects.get(id=response.data['id'])
        self.assertIsNotNone(task)
        self.assertEqual(task.title, T_TITLE)
        self.assertEqual(task.description, T_DESCR)
        tasks_serializer = TaskSerializer(task)
        self.assertEqual(response.data, tasks_serializer.data)

        task_user_serializer = TaskUserSerializer(self.test_user)
        self.assertEqual(response.data['owner'], task_user_serializer.data)

    def test_object_create_wrong(self):
        create_data_wrong = {
            'WRONG_FIELD': T_TITLE,
            'description': T_DESCR
        }
        response = self.client.post(reverse('tasks-list'), create_data_wrong)
        self.assertEqual(response.status_code, status_code.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['title'][0].code, 'required')
