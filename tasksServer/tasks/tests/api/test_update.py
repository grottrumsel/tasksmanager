# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status as status_code  # to avoid confusion with task.status

from tasks.models import Task
from tasks.serializers import TaskUserSerializer
from tasks.tests.api.commons import AuthenticatedAPITestCaseMixin, TaskAPIBaseTestCase, T_TITLE, T_DESCR


class WithTestTaskMixin(object):
    def setUp(self):
        super(WithTestTaskMixin, self).setUp()
        self.task = self._generate_tasks().pop()


class TestTaskAPIUpdate(AuthenticatedAPITestCaseMixin, WithTestTaskMixin, TaskAPIBaseTestCase):

    def test_update_object(self):
        # task created in super
        update_data = {
            'title': 'NEW TITLE',
            'description': 'NEW DESCRIPTION',
            'status': self.task.status,
        }
        response = self.client.put(reverse('task-object', kwargs={'pk': self.task.id}), update_data)
        self.assertEqual(response.status_code, status_code.HTTP_200_OK)
        task_from_db = Task.objects.get(id=self.task.id)
        self.assertEqual(task_from_db.title, update_data['title'])
        self.assertEqual(task_from_db.description, update_data['description'])
        self.assertEqual(task_from_db.status, update_data['status'])

    def test_update_object_wrong(self):

        update_data = {
            'title': 'NEW TITLE',
            # 'description' IS MISSING
            'status': self.task.status,
        }
        response = self.client.put(reverse('task-object', kwargs={'pk': self.task.id}), update_data)
        self.assertEqual(response.status_code, status_code.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['description'][0].code, 'required')


class TestTaskAPIWrongOwnerUpdate(
    AuthenticatedAPITestCaseMixin, WithTestTaskMixin, TaskAPIBaseTestCase
):

    def setUp(self):
        super(TestTaskAPIWrongOwnerUpdate, self).setUp()
        self.other_user = User.objects.create_user('other_user')

    def test_update_not_by_owner(self):

        # task prepared with user_one as owner in super

        self.client.logout()

        self.authenticate_user(self.other_user)

        update_data = {
                    'title': 'NEW TITLE',
                    'description': 'NEW DESCRIPTION',
                    'status': self.task.status,
                }
        response = self.client.put(reverse('task-object', kwargs={'pk': self.task.id}), update_data)
        self.assertEqual(response.status_code, status_code.HTTP_403_FORBIDDEN)


class TestTaskAPIChangeStatus(
    AuthenticatedAPITestCaseMixin,
    WithTestTaskMixin,
    TaskAPIBaseTestCase
):

    def setUp(self):
        super(TestTaskAPIChangeStatus, self).setUp()
        self.other_user = User.objects.create_user('other_user')

    def test_change_status(self):
        # task created in super
        self.assertEqual(self.task.status, Task.UNDONE)
        patch_data = {
            'status': Task.DONE
        }
        response =\
            self.client.patch(reverse('task-object', kwargs={'pk': self.task.id}), patch_data)
        self.assertEqual(response.status_code, status_code.HTTP_200_OK)
        self.assertEqual(response.data['status'], Task.DONE)
        self.assertEqual(response.data['status_verbose'], 'Done')

        task_user_serializer = TaskUserSerializer(self.test_user)
        self.assertEqual(response.data['doer'], task_user_serializer.data)

    def test_from_done_to_undone_by_not_owner(self):

        # prepares task
        task = self._create_test_task()
        self.assertEqual(task.status, Task.UNDONE)
        patch_data = {
            'status': Task.DONE
        }

        # changes its status
        response =\
            self.client.patch(reverse('task-object', kwargs={'pk': task.id}), patch_data)
        self.assertEqual(response.status_code, status_code.HTTP_200_OK)
        self.assertEqual(response.data['status_verbose'], 'Done')

        # its done and has test_user as owner
        self.assertEqual(response.data['status'], Task.DONE)
        task_user_serializer = TaskUserSerializer(self.test_user)
        self.assertDictEqual(response.data['doer'], task_user_serializer.data)

        # changes authenticated user
        self.client.logout()
        self.authenticate_user(self.other_user)

        # tries to change tasks status again with other user
        patch_data = {
            'status': Task.UNDONE
        }
        response = \
            self.client.patch(reverse('task-object', kwargs={'pk': task.id}), patch_data)
        self.assertEqual(response.status_code, status_code.HTTP_403_FORBIDDEN)
        task.refresh_from_db()
        # should fail to change status to undone from done by user that is not owner
        self.assertEqual(task.status, Task.DONE)


