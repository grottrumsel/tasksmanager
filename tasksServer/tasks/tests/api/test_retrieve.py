# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from rest_framework import status as status_code  # to avoid confusion with task.status

from tasks.models import Task
from tasks.serializers import TaskSerializer
from tasks.tests.api.commons import AuthenticatedAPITestCaseMixin, TaskAPIBaseTestCase


class TestTaskAPIRetrieve(AuthenticatedAPITestCaseMixin, TaskAPIBaseTestCase):

    def test_object_retrieve(self):
        task = self._generate_tasks().pop()
        response = self.client.get(reverse('task-object', kwargs={'pk': task.id}))
        self.assertEqual(response.status_code, status_code.HTTP_200_OK )
        tasks_serializer = TaskSerializer(task)
        self.assertEqual(response.data, tasks_serializer.data)
        self.assertEqual(response.data['id'], task.id)

    def test_object_retrieve_wrong(self):
        response = self.client.get(reverse('task-object', kwargs={'pk': 666}))
        self.assertEqual(response.status_code, status_code.HTTP_404_NOT_FOUND)

    def test_list_retrieve(self):
        self._generate_tasks(amount=5)
        response = self.client.get(reverse('tasks-list'))
        self.assertEqual(response.status_code, status_code.HTTP_200_OK )
        # taking tasks from db rather than from generating because of ordering
        tasks = Task.objects.all()
        tasks_serializer = TaskSerializer(tasks, many=True)
        self.assertEqual(response.data, tasks_serializer.data)
