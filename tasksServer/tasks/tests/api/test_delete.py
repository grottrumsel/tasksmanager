# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status as status_code  # to avoid confusion with task.status

from tasks.models import Task
from tasks.tests.api.commons import AuthenticatedAPITestCaseMixin, TaskAPIBaseTestCase


class TestTaskAPIDelete(AuthenticatedAPITestCaseMixin, TaskAPIBaseTestCase):

    def test_object_delete(self):
        task_created = self._generate_tasks().pop()
        task_in_db = Task.objects.get(id=task_created.id)
        self.assertIsNotNone(task_in_db)
        response = self.client.delete(reverse('task-object', kwargs={'pk': task_created.id}))
        self.assertEqual(response.status_code, status_code.HTTP_204_NO_CONTENT)

        with self.assertRaises(Task.DoesNotExist):
            task_in_db = Task.objects.get(id=task_created.id)

    def test_object_delete_wrong(self):
        response = self.client.delete(reverse('task-object', kwargs={'pk': 666}))
        self.assertEqual(response.status_code, status_code.HTTP_404_NOT_FOUND)


class TestTaskAPIWrongOwnerDelete(AuthenticatedAPITestCaseMixin, TaskAPIBaseTestCase):

    def setUp(self):
        super(TestTaskAPIWrongOwnerDelete, self).setUp()
        self.other_user = User.objects.create_user('other_user')

    def authenticate_other_user(self):
        self.client.force_authenticate(user=self.other_user)

    def test_delete_not_by_owner(self):
        task = self._create_test_task()
        self.client.logout()

        self.authenticate_other_user()

        response = self.client.delete(reverse('task-object', kwargs={'pk': task.id}))
        self.assertEqual(response.status_code, status_code.HTTP_403_FORBIDDEN)