# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import base64
import datetime
import json
import jwt
import mock

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status as status_code  # to avoid confusion with task.status

from tasks.tests.api.commons import TaskAPIBaseTestCase, mock_datetime_now


class TestTaskAPIAuth(TaskAPIBaseTestCase):

    def test_sign_up(self):

        with self.assertRaises(User.DoesNotExist):
            User.objects.get(username='sign_up_username')

        sign_up_data = {
            'username': 'sign_up_username',
            'password': 'somesillypassword'
        }
        response = self.client.post(reverse('simple-users'), sign_up_data)
        self.assertEqual(response.status_code, status_code.HTTP_201_CREATED)
        # to be sure password is not send back
        self.assertTrue(not 'password' in response.data)
        self.assertIsNotNone(User.objects.get(username='sign_up_username'))

    def test_auth(self):
        # check that api will not let unauthenticated user in
        response = self.client.get(reverse('tasks-list'))
        self.assertEqual(response.status_code, status_code.HTTP_401_UNAUTHORIZED)

        credentials = {
            'username': self.test_user.username,
            'password': 'user_password'
        }

        # authenticate with check of token expire time control
        mocked_datetime = datetime.datetime.now()
        # mocking built in 'now' to check if auto_add time works
        with mock_datetime_now(mocked_datetime, datetime):
            # authenticate
            response = self.client.post(reverse('token-auth'), credentials)

        self.assertEqual(response.status_code, status_code.HTTP_200_OK)

        # checking returned token
        header, payload, signature = response.data['token'].split('.')
        header_str = base64.b64decode(header)
        payload_dict = json.loads(base64.b64decode(payload))
        self.assertEqual(header_str, '{"alg":"HS256","typ":"JWT"}')
        self.assertEqual(payload_dict['username'], 'user_one')
        self.assertEqual(payload_dict['email'], '')
        self.assertEqual(payload_dict['user_id'], 1)

        # checking expiration date
        expected_expiry_date = mocked_datetime + datetime.timedelta(minutes=30)
        expiry_date_from_token = datetime.datetime.fromtimestamp(payload_dict['exp'])
        format_str = '%Y-%m-%d %H:%M:%S'
        self.assertEqual(
            expected_expiry_date.strftime(format_str),
            expiry_date_from_token.strftime(format_str)
        )

        # setting token to a header
        self.client.credentials(HTTP_AUTHORIZATION='JWT {}'.format(response.data['token']))

        # checking if authentication works
        response = self.client.get(reverse('tasks-list'))
        self.assertEqual(response.status_code, status_code.HTTP_200_OK)

        # mocking token expiration validator to check if it will cause unauthorizing
        with mock.patch('jwt.api_jwt.PyJWT._validate_exp') as mock_validate_exp:
            mock_validate_exp.side_effect = jwt.exceptions.ExpiredSignatureError
            response = self.client.get(reverse('tasks-list'))
            self.assertEqual(response.status_code, status_code.HTTP_401_UNAUTHORIZED)

