# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import mock

from django.contrib.auth.models import User

from rest_framework.test import APITestCase

from tasks.models import Task

# test literals
T_TITLE = 'title'
T_DESCR = 'description'


class AuthenticatedAPITestCaseMixin(object):
    """
    Mixin to make test calls running as an authenticated user.
    """

    def setUp(self):
        super(AuthenticatedAPITestCaseMixin, self).setUp()
        if not hasattr(self, 'client') or not hasattr(self, 'test_user'):
            raise NotImplemented('Wrong usage of {}'.format(self.__class__))

        self.client.force_authenticate(user=self.test_user)

    def authenticate_user(self, _user):
        self.client.force_authenticate(user=_user)


class TaskAPIBaseTestCase(APITestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('user_one', password='user_password')

    # shortcuts
    def _create_test_task(self, title=None):

        title = title or T_TITLE

        return Task.objects.create(
            title=title,
            description=T_DESCR,
            owner=self.test_user
        )

    def _generate_tasks(self, amount=1):
        result = []
        for item in xrange(amount):
            result.append(self._create_test_task(title='title {}'.format(item)))
        return result


real_datetime_class = datetime.datetime


def mock_datetime_now(target, dt):
    """
    Based on http://blog.xelnor.net/python-mocking-datetime/
    """

    class DatetimeSubclassMeta(type):
        @classmethod
        def __instancecheck__(mcs, obj):
            return isinstance(obj, real_datetime_class)

    class BaseMockedDatetime(real_datetime_class):
        @classmethod
        def now(cls, tz=None):
            print "MOCKED NOW"
            return target.replace(tzinfo=tz)

        @classmethod
        def utcnow(cls):
            print "MOCKED UTCNOW"
            return target

    # Python2 & Python3 compatible metaclass
    MockedDatetime = DatetimeSubclassMeta(str('datetime'), (BaseMockedDatetime,), {})

    return mock.patch.object(dt, 'datetime', MockedDatetime)