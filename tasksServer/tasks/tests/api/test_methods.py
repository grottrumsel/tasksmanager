# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from rest_framework import status as status_code  # to avoid confusion with task.status

from tasks.tests.api.commons import TaskAPIBaseTestCase, AuthenticatedAPITestCaseMixin


class BatchMethodCheckMixin(object):

    ALL_HTTP_METHODS = frozenset(('get', 'post', 'put', 'delete', 'patch', 'head', 'options'))

    def assertMethodsHaveResponseStatusCode(
            self, url_param, data=None, expected_status_code=None, methods=None
            ):
        """
        Checks all of the methods sequence if they return status codes equal to status_code.
        """
        methods = methods or self.ALL_HTTP_METHODS
        assert set(methods).issubset(self.ALL_HTTP_METHODS)
        assert hasattr(self, 'client')
        assert hasattr(self, 'assertEquals')
        for one_method in methods:
            method_to_call = getattr(self.client, one_method)
            _args = (url_param, data) if data else (url_param,)
            response = method_to_call(*_args)
            self.assertEquals(response.status_code, expected_status_code)



class TestWrongMethodsTaskAPIRetrieve(
    AuthenticatedAPITestCaseMixin,
    BatchMethodCheckMixin,
    TaskAPIBaseTestCase
):

    def test_wrong_methods(self):

        self.assertMethodsHaveResponseStatusCode(
            reverse('tasks-list'),
            expected_status_code=status_code.HTTP_405_METHOD_NOT_ALLOWED,
            methods=('put', 'patch', 'delete')
        )
        self.assertMethodsHaveResponseStatusCode(
            reverse('task-object', kwargs={'pk': 1}),  # any id
            expected_status_code=status_code.HTTP_405_METHOD_NOT_ALLOWED,
            methods=('post',)
        )

    def test_not_found_url(self):
        response = self.client.get('anything')
        self.assertEqual(response.status_code, status_code.HTTP_404_NOT_FOUND)

class TestNotAuthenticatedTaskAPIRetrieve(BatchMethodCheckMixin, TaskAPIBaseTestCase):

    def test_not_authenticated_calls(self):

        # checking ListCreate Resource
        self.assertMethodsHaveResponseStatusCode(
            reverse('tasks-list'),
            expected_status_code=status_code.HTTP_401_UNAUTHORIZED,
            methods=('get', 'post')
        )

        # checking RetrieveUpdateDelete Resource
        self.assertMethodsHaveResponseStatusCode(
            reverse('task-object', kwargs={'pk': 1}), # any id
            expected_status_code=status_code.HTTP_401_UNAUTHORIZED,
            methods=('get', 'put', 'patch', 'delete')
        )