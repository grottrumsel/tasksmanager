# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url, include

from rest_framework.urlpatterns import format_suffix_patterns

from . import api

urlpatterns = [
    url(r'(?P<pk>[0-9]+)/$', api.TaskDetail.as_view(), name='task-object'),
    url(r'$', api.TaskList.as_view(), name='tasks-list'),
]

urlpatterns = format_suffix_patterns(urlpatterns)