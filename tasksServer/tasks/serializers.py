# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from .models import Task

import logging


class TaskUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username')


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = ('id', 'added', 'title', 'description', 'status', 'status_verbose', 'owner', 'doer')
        read_only_fields = ('id', 'added', 'owner', 'status_verbose', 'doer')

    status_verbose = serializers.SerializerMethodField()

    owner = TaskUserSerializer(read_only=True)
    doer = TaskUserSerializer(read_only=True)

    def get_status_verbose(self, obj):
        return Task.task_status_map[obj.status]

    def _handle_post_save(self, context_request,  kwargs):
        kwargs['owner'] = context_request.user

    def _handle_put_save(self, context_request,  kwargs):
        self._handle_patch_save(context_request, kwargs)

    def _handle_patch_save(self, context_request,  kwargs):
        # task was undone and it is about to be done
        if self.instance.status == Task.UNDONE and \
                self.validated_data['status'] == Task.DONE:
            kwargs['doer'] = context_request.user

        # task was done but it is about to be undone
        if self.instance.status == Task.DONE and \
                self.validated_data['status'] == Task.UNDONE:
            # only owner could do that
            if self.instance.owner != context_request.user:
                raise PermissionDenied()
            kwargs['doer'] = None

    # to avoid hard in debuging self.getattr('_handle_{}_save'.format(method))
    method_2_save_handler_map = {
        'POST': _handle_post_save,
        'PUT': _handle_put_save,
        'PATCH': _handle_patch_save
    }

    def save(self, **kwargs):
        context_request = self.context['request']

        # enrich kwargs with appropriate owner or doer
        self.method_2_save_handler_map[context_request.method](self, context_request, kwargs)

        return super(TaskSerializer, self).save(**kwargs)
