# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class Task(models.Model):

    # status definitions
    DONE = 'D'
    UNDONE = 'U'

    task_status_choices = (
        (DONE, 'Done'),
        (UNDONE, 'Undone')
    )

    # for purpouse of get_status method and testing
    task_status_map = dict(task_status_choices)

    added = models.DateTimeField(auto_now_add=True)
    description = models.TextField(max_length=512)
    status = models.CharField(max_length=1, choices=task_status_choices, default=UNDONE)
    title = models.CharField(max_length=128)

    owner = models.ForeignKey(
        User,
        models.SET_NULL,
        related_name='owned_tasks',
        null=True  # still interested in tasks with owner deleted
    )

    doer = models.ForeignKey(  # who actually done the task
        User,
        models.SET_NULL,
        related_name='done_tasks',
        null=True
    )

    class Meta:
        ordering = ('-added',)
