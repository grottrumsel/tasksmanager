# Tasks

Example project for the purpouse of recruitment process.

## Installation

### client

You need to have installed:

* nodejs (>=v8.10.0)(stable)
* npm (>=v3.5.2)

Then in client directory run:
```
npm install
ng build
```
Client and server directories must stay in one folder (right where they are in repository), because client is building it's dits into server's statics.
### server

You need to have installed 

* Python2.7
* pip 

Then in server directory run:
```
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
```

## Running

In server directory run:
```
python manage.py runserver
```
Output of this command will give you an adress to which you should follow on your browser.


## Running tests

In server directory run:
```
python manage.py test
```

## Requirements coverage (with API tests)

- [DONE] There is an “Add” button to add a new todo item.
`tasks.tests.api.test_create.TestTaskAPICreate`
- [DONE] You can via the UI “Edit”, “Mark Done” or “Delete” a task.
- [DONE] Edit: Is editing the task name, description and status (done/undone) (only your own)
`tasks.tests.api.test_update.TestTaskAPIUpdate`
`tasks.tests.api.test_update.TestTaskAPIWrongOwnerUpdate`
- [DONE] Mark Done: is changing the status of a task to “done” and record who did it.
`tasks.tests.api.test_update.TestTaskAPIChangeStatus`
- [DONE] Delete: is deleting a task (only your own)
`tasks.tests.api.test_delete.TestTaskAPIDelete`

## Author

* **Pawel B.** 

## Thoughts

* [thought 1](https://thecodinglove.com/when-i-do-css)
* [thought 2](https://thecodinglove.com/when-the-deadline-is-passed)
* [thought 3](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
