import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  public user: any;

  constructor(public userService: UserService, private router: Router) { }

  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    };
  }

  login() {
    this.userService.login({username: this.user.username, password: this.user.password})
      .subscribe((loginSucceded) => {
        if (loginSucceded) {
          this.router.navigateByUrl('/dashboard');
        }
      });
    this.user.username = '';
    this.user.password = '';
  }

  signUp() {
    this.userService.signUp({username: this.user.username, password: this.user.password})
      .subscribe((signUpSucceded) => {
        if (signUpSucceded) {
          this.login();
        }
      });
  }


  // refreshToken() {
  //   this._userService.refreshToken();
  // }

  // logout() {
  //   this.userService.logout();
  // }
}
