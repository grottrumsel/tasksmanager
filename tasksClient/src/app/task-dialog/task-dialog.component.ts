import {Component, EventEmitter, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.css']
})
export class TaskDialogComponent implements OnInit {
  taskEdited = {
    title: '',
    description: '',
    status: 'U',
    id: null
  };

  public taskFormControl = new FormControl('', [
    Validators.required,
  ]);

  public event: EventEmitter<any> = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<TaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit(): void {
    if (this.data.element) {
      this.taskEdited.title = this.data.element.title;
      this.taskEdited.description = this.data.element.description;
      this.taskEdited.status = this.data.element.status;
      this.taskEdited.id = this.data.element.id;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  private isDialogReadyToSave() {
    return this.taskEdited.title && this.taskEdited.description && this.taskEdited.status;
  }

  onSubmit(): void {
    if (this.isDialogReadyToSave()) {
        this.event.emit({data: this.taskEdited});
        this.dialogRef.close();
    }
  }
}
