import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class UserService {

  // http options used for making API calls
  private httpOptions: any;

  // the actual JWT token
  public token: string = null;

  // the token expiration date
  public token_expires: Date;

  // the username of the logged in user
  public username: string;

  // the user id of the logged in user
  public userId: number;

  // error messages received from the login attempt
  public errors: any = [];

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }

  public isAuthenticated(): boolean {
    return this.token !== null;
  }

  // Uses http.post() to get an auth token from djangorestframework-jwt endpoint
  public login(user): Observable<boolean> {
    return this.http.post('/token-auth/', JSON.stringify(user), this.httpOptions)
      .pipe(
         map(
          data => {
            this.updateData(data['token']);
            return true;
          }),
        catchError(this.handleError('login', false))
    );
  }

  public signUp(userData): Observable<boolean> {
    return this.http.post('/users/', userData, this.httpOptions).pipe(
      map(() => true),
      catchError(this.handleError('login', false)));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.errors = error['error'];
      return of(result as T);
    };
  }

  public logout() {
    this.token = null;
    this.token_expires = null;
    this.username = null;
    this.userId = null;
  }

  private updateData(token) {
    this.token = token;
    this.errors = [];

    // decode the token to read the username and expiration timestamp
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.token_expires = new Date(token_decoded.exp * 1000);
    this.username = token_decoded.username;
    this.userId = token_decoded.user_id;
  }

  public prepareHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.token
    });
  }

}
