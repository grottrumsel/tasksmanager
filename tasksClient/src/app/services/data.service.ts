import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, filter, map, tap} from 'rxjs/operators';
import {Task} from '../Task';
import {UserService} from './user.service';

interface HttpOptions {
  headers: HttpHeaders;
}

const T_URL = '/tasks/';

@Injectable()
export class DataService {

  constructor(private httpClient: HttpClient, private userService: UserService) {
  }

  getTasks(withDone): Observable<Task[]> {
    return this.httpClient.get<Task[]>(T_URL, this.prepareHttpOptions())
      .pipe(
        map((tasks: Task[]) =>
          tasks.filter( (task: Task) => {
          if (!withDone) {
            return task.status === 'U';
          } else {
            return true;
          }
        })),
        catchError(this.handleError('getTasks', []))
      );
  }

  addTask(newTaskData): Observable<Task> {
      return this.httpClient.post<Task>(T_URL, newTaskData, this.prepareHttpOptions())
          .pipe(
            catchError(this.handleError('addTask', null))
          );
  }

  updateTask(taskData: Task): Observable<Task> {
      return this.httpClient.put<Task>(`${T_URL}${taskData.id}/`, taskData, this.prepareHttpOptions())
          .pipe(
            catchError(this.handleError('updateTask', null))
          );
  }

  deleteTask(taskId) {
    return this.httpClient.delete(`${T_URL}${taskId}/`, this.prepareHttpOptions())
      .pipe(
        catchError(this.handleError('deleteTask', null))
      );
  }

  changeStatus(taskId, status): Observable<Task> {
    return this.httpClient.patch<Task>(
      `${T_URL}${taskId}/`,
     {status: status},
          this.prepareHttpOptions()
    ).pipe(
      catchError(this.handleError('changeStatus', null))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      return of(result as T);
    };
  }

  private prepareHttpOptions(): HttpOptions {
    return {headers: this.userService.prepareHeaders()};
  }


}
