import {Component} from '@angular/core';
import {DataService} from '../services/data.service';
import {Task} from '../Task';
import {DataSource} from '@angular/cdk/table';
import {Observable} from 'rxjs/Observable';
import {UserService} from '../services/user.service';

import {TaskDialogComponent} from '../task-dialog/task-dialog.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  showsAllTasks = true;
  displayedColumns = ['added', 'reporter', 'doneby', 'status', 'title', 'description', 'actions'];
  dataSource = new TaskDataSource(this.dataService, this.showsAllTasks);

  constructor(private dataService: DataService, public dialog: MatDialog, public userService: UserService) {
  }

  toggleShowsAllTasks(): void {
    this.showsAllTasks = !this.showsAllTasks;
    this.refreshTasks()
  }

  openDialogToCreate(): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '600px',
      data: {title: 'Add Task'}
    });
    dialogRef.componentInstance.event.subscribe((result) => {
      this.dataService.addTask(result.data).subscribe(
        (newTask: Task) => {
          if (!newTask) {
            alert('There was an error while trying to add new task.');
          } else {
            // reload data after response
            this.dataSource = new TaskDataSource(this.dataService, this.showsAllTasks);
          }
        });
    });
  }
  openDialogToEdit(element: Task): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '600px',
      data: {title: 'Edit Task', element: element}
    });
    dialogRef.componentInstance.event.subscribe((result) => {
      this.dataService.updateTask(result.data).subscribe(
        (newTask: Task) => {
          if (!newTask) {
            alert('There was an error while trying to add new task.');
          } else {
            // reload data after response
            this.dataSource = new TaskDataSource(this.dataService, this.showsAllTasks);
          }
        });
    });
  }

  deleteTask(taskId) {
    if (this.userService.isAuthenticated() && confirm('Are you sure?')) {
      this.dataService.deleteTask(taskId).subscribe(() => {
          this.refreshTasks();
      });
    } else {
      alert('Login in Before');
    }
  }

  markDone(taskId) {
      if (this.userService.isAuthenticated()) {
        this.dataService.changeStatus(taskId, 'D').subscribe((result) => {
          this.refreshTasks();
        });
      } else {
        alert('Login in Before');
      }
  }
  shouldDoneButtonBeVisible(element: Task): boolean {
    return element.status !== 'D';
  }
  shouldEditButtonBeVisible(element: Task): boolean {
    return this.isUserAnOwner(element);
  }
  shouldDeleteButtonBeVisible(element: Task): boolean {
    return this.isUserAnOwner(element);
  }
  private isUserAnOwner(element: Task): boolean {
    return element.owner && element.owner.id === this.userService.userId;
  }

  private refreshTasks(): void {
    this.dataSource = new TaskDataSource(this.dataService, this.showsAllTasks);
  }
}

export class TaskDataSource extends DataSource<any> {


  constructor(private dataService: DataService, private withDone) {
    super();
  }

  connect(): Observable<Task[]> {
    return this.dataService.getTasks(this.withDone);
  }

  disconnect() {
  }
}
