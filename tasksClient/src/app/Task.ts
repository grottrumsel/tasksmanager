export interface TaskUser {
  id: number;
  username: string;
}

export interface Task {
    id: string;
    title: string;
    description: string;
    added: Date;
    status: string;
    status_verbose: string;
    owner: TaskUser;
    doer: TaskUser;
}
